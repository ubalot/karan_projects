import math
import sys

if len(sys.argv) <= 1:
    print("pass a digit as argument")
    sys.exit(1)

digit = int(sys.argv[1])

if digit > 50:
    print("insert a digit < 50")
    sys.exit(2)

for i, d in enumerate(str(math.pi)[2:]):
    if i == digit:
        print(d)

sys.exit(0)
