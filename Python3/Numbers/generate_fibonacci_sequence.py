import sys


def fibonacci_to(number):
    m, n = 1, 1
    results = [n, m]
    
    while True:
        t = m + n
        if t > number:
            break
        m = n
        n = t
        results.append(n)

    return results


def enumerate_fibonacci(number):
    m, n = 1, 1
    for i in range(1, number):
        t = m + n
        m = n
        n = t
        if i == number:
            return n
    return n


if len(sys.argv) <= 1:
    print("insert a positive integer")
    sys.exit(1)

number = int(sys.argv[1])

fibo_list = fibonacci_to(number)

print(fibo_list)

result = enumerate_fibonacci(number)

print(result)

sys.exit(0)

