import sys


def is_prime(n):
    if n % 2 == 0 and n > 2:
        return False

    for d in range(3, n, 2):
        if n % d == 0:
            return False
    
    return True


def prime_factors(number):
    prime_factors = []
    
    for i in range(2, number + 1):
        if number % i == 0 and is_prime(i):
            prime_factors.append(i)
    
    return prime_factors


if len(sys.argv) < 2:
    print("insert a positive ingteger")
    sys.exit(1)

number = int(sys.argv[1])

factors = prime_factors(number)

print(factors)

sys.exit(0)

