import sys

primes = [2]

def next_prime():
    m = primes[-1]
    for n in range(m, (m * 2) + 1):
        if not any(p for p in primes if n % p == 0):
            primes.append(n)
            return n

print(2)
answer = input("Do you want to stop? y/[n]: ")
if answer.lower() == 'y' or answer.lower() == 'yes':
    print()
    sys.exit(1)

while True:
    print(next_prime())
    print()

    answer = input("Do you want to stop? y/[n]: ")
    if answer.lower() == 'y' or answer.lower() == 'yes':
        print()
        break

