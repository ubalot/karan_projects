import System.IO
import System.Environment

-- Print nth decimal digit of euler number
evaluate :: Int -> IO ()
evaluate n = do
  let e = exp 1
  let euler = show e
  let decimals = drop 2 euler
  -- putStrLn (decimals)
  putStrLn (enumerate (n-1) decimals)
  where
    enumerate n (x:xs) =
      case n of
        0 -> [x]
        _ -> (enumerate (n - 1) xs)


main :: IO ()
main = do
  args <- getArgs
  case args of
    [arg] -> evaluate (read arg)
    _ -> putStrLn "Insert a number."